module.exports = {
  /* Use the Desy default config */
  presets: [require('desy-html/config/tailwind.config.js')],
  /* Change PurgeCSS files to add DESY AND this project's files */
  purge: {
    content: ['./node_modules/desy-html/src/**/*.html',
              './node_modules/desy-html/src/**/*.njk',
              './src/**/*.html',
              './src/**/*.njk',
              './docs/**/*.html',
              './docs/**/*.njk'
              ],
    options: {
      safelist: [
                  'has-offcanvas-open',
                  'has-dialog',
                  'dialog-backdrop',
                  'dialog-backdrop.active',
                  'focus',
                  'dev'
                  ],
    }
  },
  variants: {
    extend: {
      accessibility: ['group-hover'],
    }
  }
}
