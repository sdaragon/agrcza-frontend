# agrcza-frontend
![Version](https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000)
![Prerrequisito](https://img.shields.io/badge/npm-%3E%3D6.14.0-blue.svg)
![Prerrequisito](https://img.shields.io/badge/node-%3E%3D12.18.0-blue.svg)
[![Licencia: EUPL--1.2](https://img.shields.io/badge/License-EUPL--1.2-yellow.svg)](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12)

> agrcza-frontend es el repo de que usa desy-html para gestionar los sorteos de licencia de Caza en Aragón. Una aplicación web del Gobierno de Aragón.

## Sitio web
[https://desy.aragon.es/](https://desy.aragon.es/)

## Repositorio
[https://bitbucket.org/sdaragon/agrcza-frontend/](https://bitbucket.org/sdaragon/agrcza-frontend/)

## Prerrequisitos

- npm >=6.14.0
- node >=12.18.0

## Instalación

```sh
npm install
```

## Primeros pasos

Descárgate o haz un fork de este repositorio. Personalízalo cambiando el nombre y, si lo necesitas, las rutas de compilación. 

Para compilar el proyecto para producción en la carpeta /dist: compila el HTML, CSS purgeado y minificado para producción y Javascript:

```sh
npm run prod
```

Para desarrollar el proyecto: compila el HTML, CSS con todas las clases de Tailwind CSS y Javascript y escucha cambios en los archivos con Browser sync. Para ver el resultado abre una ventana del navegador e introduce la dirección http://localhost:3000/

```sh
npm run dev
```

## Autor

SDA Servicios Digitales de Aragón


## Licencia

Este proyecto tiene la licencia [EUPL--1.2](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12)